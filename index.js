const fs = require("fs");
const path = require('path');
const tls = require('tls');
const Buffer = require('buffer');
const crypto = require('crypto');

const options = {
    key: fs.readFileSync('cert\\server-key.pem'),
    cert: fs.readFileSync('cert\\server-crt.pem'),
    ca: fs.readFileSync('cert\\ca-crt.pem'),
    requestCert: true,
    rejectUnauthorized: true
};

const clients = {
    '3F:AF:E5:E9:9D:2E:64:4F:1C:79:B1:C4:73:5F:E4:74:24:09:96:15': 1
};

const server = tls.createServer(options, (socket) => {
    console.log('server connected',
        socket.authorized ? 'authorized' : 'unauthorized');

    const cert = socket.getPeerCertificate();
    const clientId = clients[cert.fingerprint];

    socket.on('data', (data) => {
        const dataObj = JSON.parse(Buffer.Buffer.from(data.toString(), 'base64').toString());
        if (dataObj.command === 'post') {
            const id = saveData(dataObj, clientId);
            socket.write(id);
        } else if (dataObj.command === 'get') {
            try {
                const fileContent = loadData(dataObj.fileId, clientId);
                const dataForSent = {
                    success: true,
                    data: JSON.parse(Buffer.Buffer.from(fileContent, 'base64').toString())
                };
                socket.write(Buffer.Buffer.from(JSON.stringify(dataForSent)).toString('base64'));
            } catch (e) {
                socket.write(e.message());
            }
        }
    });

    socket.on('error', (error) => {
        console.log(error);
    });

    socket.pipe(socket);
});

server.listen(8000, () => {
    console.log('server started');
});

function saveData(fileData, clientId) {
    const hash = crypto.createHash('md5');
    hash.update(JSON.stringify(fileData));
    const id = hash.digest('hex');
    const data = {
        fileName: fileData.fileName,
        content: fileData.content,
    };
    const fileContent = Buffer.Buffer.from(JSON.stringify(data)).toString('base64');
    const fname = path.resolve(path.join('storage', 'client' + clientId, id + '.dat'));
    fs.writeFileSync(fname, fileContent);

    return id;
}

function loadData(fileId, clientId) {
    const fname = path.resolve(path.join('storage', 'client' + clientId, fileId + '.dat'));
    const content = fs.readFileSync(fname);

    return content.toString();
}
