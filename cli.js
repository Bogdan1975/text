const fs = require("fs");
const path = require('path');
const tls = require('tls');
const Buffer = require('buffer');
const NodeRSA = require('node-rsa');

const tlsOptions = {
    ca: fs.readFileSync('cert\\ca-crt.pem'),
    key: fs.readFileSync('cert\\client1-key.pem'),
    cert: fs.readFileSync('cert\\client1-crt.pem'),
    host: 'localhost',
    port: 8000,
    rejectUnauthorized:true,
    requestCert:true
};

const arguments = process.argv.slice(2);

main(arguments);

function main(args) {

    if (args && args[0].toLowerCase() === 'post') {
        if (!args[1]) {
            error(`Second argument should be a valid filename`);
        }
        post(path.resolve(args[1]));
    } else if (args && args[0].toLowerCase() === 'get') {
        if (!args[1]) {
            error(`Second argument should be a file ID`);
        }
        get(args[1]);
    } else {
        error(`First argument should be 'post' or 'get'`);
    }
}

function error(text) {
    console.error(text);
    process.exit();
}

function resolvePeerForPost(fileName) {
    return tls.connect(tlsOptions);
}

function post(fileName) {
    const socket = resolvePeerForPost(fileName);
    fs.readFile(fileName, function (err, fileBuffer) {
        if (err) {
            console.log(err);
            error(err.message);
        }
        const data = preparePostData(fileName, encryptBuffer(fileBuffer));
        socket.write(data);
        socket.on('data', (data) => {
            console.log('Your file ID: ' + data.toString());
            socket.end();
            process.exit();
        });

    });
}

function resolvePeerForGet(fileId) {
    return tls.connect(tlsOptions);
}

function get(fileId) {
    const socket = resolvePeerForGet(fileId);
    socket.write(prepareGetData(fileId));
    socket.on('data', (data) => {
        const response = JSON.parse(Buffer.Buffer.from(data.toString(), 'base64').toString());
        if (response.success) {
            saveToInbox(response.data);
            console.log(`File '${data.fileName}' was saved to inbox`);
        } else {
            console.log(data.error);
        }
        socket.end();
        process.exit();
    });
}

function prepareGetData(fileId) {
    const data = {
        command: 'get',
        fileId
    };

    return Buffer.Buffer.from(JSON.stringify(data)).toString('base64');
}

function preparePostData(fileName, fileContent) {
    const data = {
        command: 'post',
        fileName: path.basename(fileName),
        content: fileContent
    };

    return Buffer.Buffer.from(JSON.stringify(data)).toString('base64');
}

function saveToInbox(fileData) {
    const fileName = fileData.fileName;
    const content = fileData.content;
    const key = new NodeRSA();
    key.importKey(fs.readFileSync('cert\\client1-key.pub'), 'public');
    key.importKey(tlsOptions.key, 'private');
    const buffer = Buffer.Buffer.from(content, 'base64');
    const decripted = key.decrypt(buffer);
    const filePath = path.resolve(path.join('inbox'), fileName);
    fs.writeFileSync(filePath, decripted);
}

function encryptBuffer(buffer) {
    const key = new NodeRSA();
    key.importKey(fs.readFileSync('cert\\client1-key.pub'), 'public');
    key.importKey(tlsOptions.key, 'private');

    return key.encrypt(buffer, 'base64');
}

